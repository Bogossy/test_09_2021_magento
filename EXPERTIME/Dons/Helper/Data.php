<?php

namespace EXPERTIME\Dons\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\ObjectManagerInterface;
use \Magento\Checkout\Model\Cart ;
use \Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Directory\Model\CurrencyFactory;
use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\Quote\Model\QuoteFactory;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Convert\Order;
use Magento\Sales\Model\Order\Shipment;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Quote\Model\Quote\Item\ToOrderItem ;

class Data extends AbstractHelper
{
    protected $storeManager;
    protected $objectManager;
    protected $cart;
    protected $currencyFactory;
    protected $productFactory;
    protected $quoteFactory;
    protected $orderRepository;
    protected $convertOrder;
    protected $shipmentCollection;
    protected $productHelper;
    protected $orderCollectionFactory;
    protected $_toOrderItem;

    /**
     * @var String
     */
    protected $exceptionMessage;


    public function __construct(Context $context,
    Cart $cart,
    PriceCurrencyInterface $priceCurrency, 
    ObjectManagerInterface $objectManager, 
    StoreManagerInterface $storeManager,
    ScopeConfigInterface $scopeInterface,
    CurrencyFactory $currencyFactory,
    ProductInterfaceFactory $productFactory,
    QuoteFactory $quoteFactory,
    OrderRepositoryInterface $orderRepository,
    Order $convertOrder,
    Shipment $shipmentCollection,
    CollectionFactory $orderCollectionFactory,
    ToOrderItem  $_toOrderItem
    )
    {
        $this->objectManager = $objectManager;
        $this->storeManager  = $storeManager;
        $this->cart = $cart;
        $this->quoteFactory = $quoteFactory;
        $this->orderRepository = $orderRepository;
        $this->convertOrder = $convertOrder;
        $this->shipmentCollection = $shipmentCollection;
        $this->orderCollectionFactory = $orderCollectionFactory;
        parent::__construct($context);
    }

    public function getConfig($setvalue, $storeId = null)
    {

        return $this->scopeConfig->getValue($setvalue, ScopeInterface::SCOPE_STORE, $storeId);
    }

    public function getCart($setsku)
    {

            $q = $this->cart->getQuote();
            $Items = $q->getAllItems();

            foreach($Items as $item) {
                $sku = $item->getProduct()->getSku(); 
                //$skuconfig=getConfig("dons/general/sku");
                if ($sku==$setsku){
                    $result=true;
                } else{
                    $result=false;
                }              
            }
            return $result;
    }

    public function AddDonation($setmontant){
            // Add product(s) to quote
            $sku = 'N000-Dons';
            $currentProduct = $this->productFactory->create()->loadByAttribute("sku", $sku);
            if (!$currentProduct) {
                $product = [
                    'designation' => 'Dons',
                    'poids_net' => 1,
                    'prix_unitaire' => $setmontant,
                    'actif' => 0,
                    'stock_reservable' => 0,
                    'category_path' => '',
                    'visibilite' => \Magento\Catalog\Model\Product\Visibility::VISIBILITY_NOT_VISIBLE
                ];
                $currentProduct = $this->productHelper->createProduct($sku, $product, $websiteId);
            }
            try {
                $this->addNewItem($order, $quote, $currentProduct, 1, $setmontant, $item['taux_tva'], 'line',  'ceci est un test');
                $order->save();
            } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Unable to add product to quote : ' . PHP_EOL . $e->getMessage()));
            }
        
        try {
            $this->collectQuoteOrder($quote, $order, $templateOrder['tva']);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Unable to collect totals on quote or order : ' . PHP_EOL . $e->getMessage()));
        }
    }


    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Sales\Model\Order $order
     * @throws \Exception
     */
    protected function collectQuoteOrder($quote, $order, $tvaAmount)
    {
        $quote->getShippingAddress()->requestShippingRates();
        $shippingAmount = $quote->getShippingAddress()->getShippingAmount();
        $itemTax = 0;
        $subTotal = 0;
        foreach ($order->getAllVisibleItems() as $item) {
            $itemTax += $item->getTaxAmount();
            $subTotal += $item->getRowTotalInclTax() - $item->getTaxAmount();
        }
        $grandTotalTTC = $subTotal + $shippingAmount + (float)$tvaAmount;

        $order->setSubtotal($subTotal);
        $order->setBaseSubtotal($subTotal);
        $order->setTaxAmount((float)$tvaAmount);
        $order->setBaseTaxAmount((float)$tvaAmount);
        $order->setGrandTotal($grandTotalTTC);
        $order->setBaseGrandTotal($grandTotalTTC);
        // Save order
        try {
            $order->save();
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Unable to save order : ' . PHP_EOL . $e->getMessage()));
        }
    }

    /**
     * addNewItem
     *
     * @param \Magento\Sales\Model\Order $order
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @param $qty
     * @param $price
     * @param $taux_tva
     * @param $erpLine
     * @param $message
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function addNewItem($order, $quote, $product, $qty, $price, $taux_tva, $erpLine, $message = null)
    {
        if (!$product) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Product is not valid.'));
        }

        // Enable product before adding to quote
        try {
            $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Unable to enable product before adding to quote : ' . PHP_EOL . $e->getMessage()));
        }



        try {
            $addedProduct = $quote->addProduct(
                $product,
                (int)$qty
            );
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Error when adding product : ' . PHP_EOL . $e->getMessage()));
        }


        $TvaAmount = ($price * (float)$taux_tva) / 100;
        $priceInclTva = $price + $TvaAmount;
        $totalPrice = (int)$qty * $price;
        $totalTvaAmount = ($totalPrice * (float)$taux_tva) / 100;
        $totalPriceInclTva = $totalPrice + $totalTvaAmount;

        // Customize the price
        try {
            $addedProduct->setNoDiscount(true);
            $addedProduct->setCustomPrice($price);
            $addedProduct->setOriginalCustomPrice($price);
            $addedProduct->setPrice($price);
            $addedProduct->setPriceInclTax($priceInclTva);
            $addedProduct->setBasePrice($price);
            $addedProduct->setTaxPercent((float)$taux_tva);
            $addedProduct->setTaxAmount($totalTvaAmount);
            $addedProduct->setRowTotalInclTax($totalPriceInclTva);
            $addedProduct->setBaseRowTotalInclTax($totalPriceInclTva);
            $addedProduct->setRowTotal($totalPrice);
            $addedProduct->setBaseRowTotal($totalPrice);
            $addedProduct->getProduct()->setIsSuperMode(true);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Unable to customize product price : ' . PHP_EOL . $e->getMessage()));
        }


        // Set a custom message
        if (null !== $message) {
            try {
                $addedProduct->setMessage($message);
            } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Unable to add product\'s custom message : ' . PHP_EOL . $e->getMessage()));
            }
        }

        $quote->collectTotals()->save();

        // Update order if present
        if (null !== $order) {
            $productId = $product->getId();
            foreach ($quote->getAllVisibleItems() as $item) {
                if ($productId == $item->getProduct()->getId()) {
                    $item->save();
                    $existData = $order->getItemByQuoteItemId($item->getId());
                    $orderItem = $this->_toOrderItem->convert($item);

                    if ($existData) {
                        $existData->addData($orderItem->getData());
                    } else {
                        /**
                         * add new product to
                         */
                        if ($item->getParentItem()) {
                            $item->setParentItem(
                                $order->getItemByQuoteItemId($item->getParentItem()->getId())
                            );
                        }
                        $order->addItem($orderItem);
                    }
                }
            }
        }
    }


}