<?php
namespace EXPERTIME\Dons\Block;
use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \Magento\Catalog\Model\ProductRepository;
use \Magento\Store\Model\StoreManagerInterface;
use \EXPERTIME\Dons\Helper\Data;
class Donation extends Template
{
    protected $_productRepository;
    public function __construct(Context $context,ProductRepository $productRepository, StoreManagerInterface $storeManager, Data $helperData,array $data = [])
    {        
        $this->_storeManager = $storeManager;
        $this->_helperData = $helperData;
        $this->_productRepository = $productRepository;
        parent::__construct($context,$data);
    }
public function getConfigHelperdata($value){
        return $this->_helperData->getConfig($value); 
    }
    public function getProductBySku($sku)
	{
		return $this->_productRepository->get($sku);
	}
    public function getProductExistInCart($sku)
	{
		return $this->_helperData->getCart($sku);
	}
    public function AddQuote($montant)
	{
		return $this->_helperData->AddDonation($montant);
	}
}